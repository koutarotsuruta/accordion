var currentWidth = window.innerWidth;
var init_btn = document.querySelectorAll('.accd_btn');
var init_contents = document.querySelectorAll('.accd_contents');
var array_paddingTop = [];
var array_paddingBottom = [];

function checkTab() {
    for(var i=0; i<init_contents.length; i++){
        if(document.defaultView.getComputedStyle(init_btn[i]).display === 'block') {
            init_contents[i].style.paddingTop = '0';
            init_contents[i].style.paddingBottom = '0';
            init_contents[i].style.height = '0';
        } else {
            init_contents[i].style.paddingTop = '';
            init_contents[i].style.paddingBottom = '';
            init_contents[i].style.height = '';
        }
    }
}

for(var i=0; i<init_contents.length; i++){
    array_paddingTop.push(parseInt(document.defaultView.getComputedStyle(init_contents[i]).paddingTop));
    array_paddingBottom.push(parseInt(document.defaultView.getComputedStyle(init_contents[i]).paddingBottom));
    checkTab();
}

window.addEventListener('load',function(){    
    setTimeout(function(){
        function slideToggle(accd_contents, result, number) {
            var initHeight;
            if (!result){
                initHeight = accd_contents.scrollHeight;
                var paddingTop = (array_paddingTop[number] === undefined)? 0 : array_paddingTop[number],
                     paddingBottom = (array_paddingBottom[number] === undefined)? 0 : array_paddingBottom[number];
                
                totalHeight = initHeight + paddingTop + paddingBottom + 'px';
                accd_contents.style.padding = '';
                accd_contents.style.height = totalHeight;
            } else {
                accd_contents.style.height = '0';
                init_contents[number].style.paddingTop = '0';
                init_contents[number].style.paddingBottom = '0';
            }
        }
        var accd_btn = document.querySelectorAll('.accd_btn');
        var accd_position = [];
        for (var i = 0; i < accd_btn.length; i++) {
            accd_position.push();
            accd_btn[i].onclick = function () {
                var arrayNumber = [].indexOf.call(accd_btn, this);
                var accd_contents;
                var prevIs = true;
                var errorPrevious = false,
                    errorNext = false;
                
                try {
                    this.previousElementSibling.classList.contains('accd_contents');
                } catch (e) {
                    errorPrevious = true;
                } finally {
                    if (errorPrevious === false) {
                        accd_contents = this.previousElementSibling;
                    }
                }
                
                try {
                    this.nextElementSibling.classList.contains('accd_contents');
                } catch (e) {
                    errorNext = true;
                } finally {
                    if (errorNext === false) {
                         prevIs = false;
                        accd_contents = this.nextElementSibling;
                    }
                }
                
                var result;
                if (this.classList.contains('active')) {
                    this.classList.remove('active');
                    accd_contents.classList.remove('active');
                    if(prevIs === true) {
                        window.scrollTo(0,window.pageYOffset + accd_contents.getBoundingClientRect().top - 200); 
                    }
                    result = true;
                } else {
                    this.classList.add('active');
                    accd_contents.classList.add('active');
                    result = false;
                }
                slideToggle(accd_contents, result, arrayNumber);
            }
        }
    },500);
});

window.addEventListener('resize',function(){
    if (currentWidth != window.innerWidth) {
        checkTab();

        for(var i=0; i<init_btn.length; i++) {
            var errorPrevious = false,
                    errorNext = false;
            
            try {
               init_btn[i].previousElementSibling.classList.remove('active');	
            } catch (e) {
                errorPrevious = true;
            } finally {
                if (errorPrevious === false) {
                    init_btn[i].previousElementSibling.classList.remove('active');	
                    init_btn[i].classList.remove('active');	
                }
            }

            try {
                init_btn[i].nextElementSibling.classList.contains('active');
            } catch (e) {
                errorNext = true;
            } finally {
                if (errorNext === false) {
                    init_btn[i].nextElementSibling.classList.remove('active');	
                    init_btn[i].classList.remove('active');
                }
            }
        }
    }
});
